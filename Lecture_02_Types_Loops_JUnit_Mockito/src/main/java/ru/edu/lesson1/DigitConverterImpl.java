package ru.edu.lesson1;

public class DigitConverterImpl implements DigitConverter {
    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(int digit, int radix) {
        if (digit < 0) throw new IllegalArgumentException("Неверное значение digit");
        if (radix < 1) throw new IllegalArgumentException("Неверное значение radix");
        if (digit == 0) {
            return "0";
        }

        int tmp = digit;
        String result = "";
        while (tmp > 0) {
            result = String.valueOf(tmp % radix) + result;
            tmp = tmp / radix;
        }
        return result;
    }
}
