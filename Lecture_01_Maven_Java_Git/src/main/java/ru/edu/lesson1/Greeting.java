package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Greeting implements IGreeting {

    public List<Hobby> hobbies = new ArrayList<>();
    private String firstname = "Aleksandr";
    private String secondName = "Mishukov";
    private String lastName = "Valentinovich";
    private String bitbucketUrl = "https://bitbucket.org/entrery/";
    private String phone = "+79169259229";
    private String courseExpectation = "Knowledge";
    private String educationInfo = "NIB";

    public Greeting() {
        hobbies.add(new Hobby("1", "Games", "Interactive games like a monopoly"));
    }

    /**
     * Get first name.
     */
    @Override
    public String getFirstName() {
        return firstname;
    }

    /**
     * Get second name
     */
    @Override
    public String getSecondName() {
        return secondName;
    }

    /**
     * Get last name.
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Get hobbies.
     */
    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    /**
     * Get bitbucket url to your repo.
     */
    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    /**
     * Get phone number.
     */
    @Override
    public String getPhone() {
        return phone;
    }

    /**
     * Your expectations about course.
     */
    @Override
    public String getCourseExpectation() {
        return courseExpectation;
    }

    /**
     * Print your university and faculty here.
     */
    @Override
    public String getEducationInfo() {
        return educationInfo;
    }

    @Override
    public String toString() {
        return "Greeting{" +
                "hobbies=" + hobbies +
                ", firstname='" + firstname + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", bitbucketUrl='" + bitbucketUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", courseExpectation='" + courseExpectation + '\'' +
                ", educationInfo='" + educationInfo + '\'' +
                '}';
    }
}
