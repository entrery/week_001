import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.Greeting;
import ru.edu.lesson1.IGreeting;
import ru.edu.lesson1.Hobby;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class GreetingsTests {

    private IGreeting greeting = new Greeting();

    @Test
    public void size_Test() {

        Assert.assertNotNull(greeting);

        assertEquals("Aleksandr", greeting.getFirstName());

        assertEquals("Mishukov", greeting.getSecondName());

        assertEquals("Valentinovich", greeting.getLastName());

        assertEquals("https://bitbucket.org/entrery/", greeting.getBitbucketUrl());

        assertEquals("+79169259229", greeting.getPhone());

        assertEquals("Knowledge", greeting.getCourseExpectation());

        assertEquals("NIB", greeting.getEducationInfo());

        assertNotNull(greeting.getHobbies());

        assertNotNull(greeting.toString());




        // check your methods
    }
}
